/* 
 * compile this program like this,
 *     $ gcc $(pkg-config --cflags --libs libxml-2.0 glib-2.0) -o xmltree xmltree.c
 */
#include <stdio.h>
#include <glib-unix.h>
#include <libxml/xmlreader.h>
#include <string.h>

static void printNode(xmlTextReaderPtr reader)
{
  gchar *name = NULL;
  gchar *value = NULL;
  gchar **valuev = NULL;
  gchar *valuej = NULL;
  gint depth = -1;
  gchar *depthstring = NULL;

      name = xmlTextReaderName(reader);
      value = xmlTextReaderValue(reader);
      if(value && strlen(value))
	{
	  valuev = g_strsplit(value, "\n", 0);
	  valuej = g_strjoinv(" ", valuev);
	}

      depth = xmlTextReaderDepth(reader);
      depthstring = g_strnfill(depth, '\t');

      if(valuej)
	{
	  g_strstrip(valuej);
	  if(strlen(valuej))
	    g_print("%s[%d] %s = %s\n", depthstring, depth, name, valuej);
	}
      else if(g_strcmp0("#text", name) != 0)
	g_print("%s[%d] <%s>\n", depthstring, depth, name);

      g_free(name);
      g_free(value);
      g_strfreev(valuev);
      g_free(valuej);
      g_free(depthstring);
}

static void processNode(xmlTextReaderPtr reader)
{
  if(xmlTextReaderNodeType(reader) != XML_READER_TYPE_END_ELEMENT)
    {
      printNode(reader);
      if(xmlTextReaderHasAttributes(reader))
	{
	  while(xmlTextReaderMoveToNextAttribute(reader))
	    printNode(reader);
	}
    }
}
 
int main(int argc, char **argv)
{
  gchar *filename = NULL;
  xmlTextReaderPtr reader = NULL;
  int ret = 0;

  if(argc < 2)
    exit(EXIT_FAILURE);

  filename = g_strdup(argv[1]);

  reader = xmlNewTextReaderFilename(filename);
  if(!reader)
    exit(EXIT_FAILURE);

  while((ret = xmlTextReaderRead(reader)))
	processNode(reader);

  
  xmlFreeTextReader(reader);
  return(0);
}
