# Makefile for xmltree.c

MODULES := libxml-2.0 glib-2.0
SOURCES := xmltree.c
BINARY := xmltree
CC := gcc

CFLAGS += $(shell pkg-config --cflags $(MODULES)) -I. -O0 -ggdb
LIBS += $(shell pkg-config --libs $(MODULES))

OBJECTS := $(SOURCES:%.c=%.o)

ALL: $(BINARY)

$(BINARY) : $(OBJECTS)
	$(CC) $(LIBS) -o $(BINARY) $(OBJECTS)

%.o : %.c
	$(CC) $(CFLAGS) -c -o $@ $<

clean:
	rm -fr $(OBJECTS) $(BINARY)

.PHONY:
	clean